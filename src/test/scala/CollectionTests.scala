import findexedDB.model.{Col, Collection, ForeignKey, Key}
import utest._

object CollectionTests extends TestSuite {
    val tests: Tests = Tests {
        // not a common practice to have shared Keys among different collections, used here to avoid boilerplate
        def intKey: Key[Int] = Col[Int]("intKey")
        def intPrimaryKey: Key[Int] = Col[Int]("intPrimaryKey", Col.IsPrimaryKey)
        def stringKey: Key[String] = Col[String]("stringKey")

        /**
         * Creating simple collection with two keys ans asserting all its schema values
         */
        test("CollectionCreation") {
            val keys = (intKey, stringKey)
            val constraints = Seq()
            val collection = Collection[(Int, String)]("SimpleCollection")(keys)(constraints)

            // checking that collection schema has correct name
            collection.schema.title ==> "SimpleCollection"

            // checking all added keys (should be two keys)
            collection.schema.keys.length ==> 2
            collection.schema.keys.head.title ==> "intKey"
            collection.schema.keys(1).title ==> "stringKey"

            // we didn't specify any Primary Key
            collection.schema.getPrimaryKeyTitle.isEmpty ==> true

            // there should be no constraints in this collection
            collection.schema.constraints.isEmpty ==> true
        }

        /**
         * We can mark some Keys of collection as "Primary"
         */
        test("PrimaryKeyInCollection") {
            val keys = (intPrimaryKey, stringKey)
            val constraints = Seq()
            val collection = Collection[(Int, String)]("CollectionWithPrimaryKey")(keys)(constraints)

            collection.schema.keys.head.isPrimary ==> true
            collection.schema.getPrimaryKeyTitle ==> Some("intPrimaryKey")
        }

        /**
         * Adding a simple constraint as a Foreign Key
         */
        test("ForeignKeyConstraint") {
            val keys = (intKey, stringKey)
            // means CollectionWithConstraint.intPrimaryKey -> SomeDifferentCollection.intKey
            val constraints = Seq(ForeignKey(intKey, "SomeDifferentCollection", intPrimaryKey))
            val collection = Collection[(Int, String)]("CollectionWithConstraint")(keys)(constraints)

            collection.schema.constraints.length ==> 1
            collection.schema.constraints.head.referencingCollection ==> "CollectionWithConstraint"
            collection.schema.constraints.head.referencingKey ==> "intKey"
            collection.schema.constraints.head.referencedCollection ==> "SomeDifferentCollection"
            collection.schema.constraints.head.referencedKey ==> "intPrimaryKey"
        }
    }
}