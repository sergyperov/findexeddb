package mocks

import cats.effect.IO
import findexedDB.model.DatabaseSchema

class ConstraintDriverMock(override val dbSchema: DatabaseSchema) extends DriverMock(dbSchema) {
    override val schema: DatabaseSchema = dbSchema

    override def beforeInsertHook(tableName: String, recordToInsert: Seq[(String, Any)]): IO[Unit] = {
        super.onMethodCall("beforeInsertHook")
        IO.unit
    }

    override def afterInsertHook(tableName: String, recordToInsert: Seq[(String, Any)]): IO[Unit] = {
        super.onMethodCall("afterInsertHook")
        IO.unit
    }

    override def beforeDeleteHook(tableName: String, keyToDelete: Any): IO[Unit] = {
        super.onMethodCall("beforeDeleteHook")
        IO.unit
    }

    override def afterDeleteHook(tableName: String, keyToDelete: Any): IO[Unit] = {
        super.onMethodCall("afterDeleteHook")
        IO.unit
    }
}
