package mocks

import cats.effect.IO
import findexedDB.core.driver.BasicDriver
import findexedDB.model.DatabaseSchema

class DriverMock(val dbSchema: DatabaseSchema) extends BasicDriver {
    override val schema: DatabaseSchema = dbSchema
    private val methodCallsCount = collection.mutable.Map[String, Int]()
    protected def onMethodCall(methodName: String): Unit = {
        val count = methodCallsCount.getOrElseUpdate(methodName, 0)
        methodCallsCount(methodName) = count + 1
    }

    def methodCallCount(methodName: String): Int = {
        methodCallsCount.getOrElse(methodName, 0)
    }

    def clearStats(): Unit = {
        methodCallsCount.clear()
    }

    def run(): IO[Unit] = IO.unit

    def insert(tableName: String, recordToInsert: Seq[(String, Any)]): IO[Unit] = {
        onMethodCall("insert")
        IO.unit
    }

    def multipleInsert(tableName: String, recordsToInsert: List[Seq[(String, Any)]]): IO[Unit] = {
        onMethodCall("multipleInsert")
        IO.unit
    }

    def delete(tableName: String, keyToDelete: Any): IO[Unit] = {
        onMethodCall("delete")
        IO.unit
    }

    def getAll(tableName: String): IO[List[Seq[(String, Any)]]] = {
        onMethodCall("getAll")
        IO {List()}
    }

    override def filter(tableName: String, predicate: Seq[(String, Any)] => Boolean): IO[List[Seq[(String, Any)]]] = {
        onMethodCall("filter")
        IO {List()}
    }

    def exists(tableName: String, f: Seq[(String, Any)] => Boolean): IO[Boolean] = {
        onMethodCall("exists")
        IO {false}
    }

    def all(tableName: String, f: Seq[(String, Any)] => Boolean): IO[Boolean] = {
        onMethodCall("all")
        IO {false}
    }
}
