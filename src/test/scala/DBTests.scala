import findexedDB.model.{Col, Collection, DatabaseSchema, ForeignKey, Key, Constraint}
import utest._

object DBTests extends TestSuite {
    val tests: Tests = Tests {
        def intKey: Key[Int] = Col[Int]("intKey")
        def intPrimaryKey: Key[Int] = Col[Int]("intPrimaryKey", Col.IsPrimaryKey)
        def stringKey: Key[String] = Col[String]("stringKey")
        def otherStringKey: Key[String] = Col[String]("otherStringKey")

        val firstCollectionKeys = (intPrimaryKey, stringKey)
        val firstCollection = Collection[(Int, String)]("FirstCollection")(firstCollectionKeys)(Seq())

        val secondCollectionKeys = (intKey, otherStringKey)
        val secondCollection = Collection[(Int, String)]("SecondCollection")(secondCollectionKeys)(Seq(
            ForeignKey(intKey, "FirstCollection", intPrimaryKey)
        ))

        val dbSchema = DatabaseSchema("some database", 1, Seq(
            firstCollection.schema,
            secondCollection.schema
        ))

        /**
         * Simple DB Schema creation, checking that all the collections fit in
         */
        test("DBCreation") {
            dbSchema.version ==> 1
            dbSchema.title ==> "some database"
            dbSchema.collections.length ==> 2
            dbSchema.collections.head ==> firstCollection.schema
            dbSchema.collections(1) ==> secondCollection.schema
        }

        /**
         * On the same collection checking that
         */
        test("Constrains") {
            dbSchema.referencedConstraints("FirstCollection") ==> Seq(
                Constraint("SecondCollection", "intKey", "FirstCollection", "intPrimaryKey")
            )
            dbSchema.referencedConstraints("SecondCollection") ==> Seq(
                /* no constrains here */
            )

            dbSchema.referencingConstraints("FirstCollection") ==> Seq(
                /* no constrains here */
            )
            dbSchema.referencingConstraints("SecondCollection") ==> Seq(
                Constraint("SecondCollection", "intKey", "FirstCollection", "intPrimaryKey")
            )
        }
    }
}
