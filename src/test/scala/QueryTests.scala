import IOSupport.IOTestSuite
import findexedDB.model.{Col, Collection, DatabaseSchema, Key}
import cats.effect.IO
import cats.implicits._
import mocks.{ConstraintDriverMock, DriverMock}
import utest._

object QueryTests extends IOTestSuite {
    private def intKey: Key[Int] = Col[Int]("intKey")
    private def stringKey: Key[String] = Col[String]("stringKey")
    private val collection = Collection[(Int, String)]("SimpleCollection")(intKey, stringKey)(Seq())

    def getDriver: DriverMock = {
        new DriverMock(DatabaseSchema("Sample database", 1, Seq(collection.schema)))
    }

    def getConstraintDriver: ConstraintDriverMock = {
        new ConstraintDriverMock(DatabaseSchema("Sample database", 1, Seq(collection.schema)))
    }

    override val allowNonIOTests = true

    val tests: Tests = Tests {
        test("getAll") {
            implicit val driver: DriverMock = getDriver
            val getAllQuery = collection.query.all[(Int, String)]
            getAllQuery >> IO(driver.methodCallCount("getAll") ==> 1)
        }

        test("insert") {
            implicit val driver: DriverMock = getDriver
            val insertQuery = collection.query += (1, "Testing")
            insertQuery >> IO(driver.methodCallCount("insert") ==> 1)
        }

        test("multipleInsert") {
            implicit val driver: DriverMock = getDriver
            val multipleInsertQuery = collection.query ++= Seq(
                (1, "Hello"),
                (2, "world")
            )
            multipleInsertQuery >> IO(driver.methodCallCount("multipleInsert") ==> 1)
        }

        test("delete") {
            implicit val driver: DriverMock = getDriver
            val deleteQuery = collection.query -= 1
            deleteQuery >> IO(driver.methodCallCount("delete") ==> 1)
        }

        /**
         * Checking that hooks are working
         */
        test("insertWithHooks") {
            implicit val driver: ConstraintDriverMock = getConstraintDriver
            val deleteQuery = collection.query += (1, "Testing")
            deleteQuery >> IO {
                driver.methodCallCount("beforeInsertHook") ==> 1
                driver.methodCallCount("insert") ==> 1
                driver.methodCallCount("afterInsertHook") ==> 1
            }
        }

        test("deleteWithHooks") {
            implicit val driver: ConstraintDriverMock = getConstraintDriver
            val deleteQuery = collection.query -= 1
            deleteQuery >> IO {
                driver.methodCallCount("beforeDeleteHook") ==> 1
                driver.methodCallCount("delete") ==> 1
                driver.methodCallCount("afterDeleteHook") ==> 1
            }
        }
    }
}
