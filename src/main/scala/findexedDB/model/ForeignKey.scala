package findexedDB.model

/**
 * Representation of ForeignKey that is used in outer API, has basic type checking
 * @param referencingKey Key which is gonna to be referencing, Collection will be automatically detected by the schema
 * @param referencedCollection collection, which has referencedKey (we need to specify this explicit because Key is an immutable data type and we can't detect Key's collection automatically)
 * @param referencedKey Key which is gonna to be referenced by the referencingKey
 * @tparam T Any type, used to ensure that referencing and referenced has the same type
 */
case class ForeignKey[T](
    referencingKey: Key[T],
    referencedCollection: String,
    referencedKey: Key[T]
) extends CollectionOption

/**
 * Most straightforward representation of ForeignKey class, only with strings
 * @param referencingCollection While ForeignKey lives within Collection, Constraint lives within DatabaseSchema so we need to explicit specify referencing collection
 * @param referencingKey Representation of [[ForeignKey.referencingKey]] but only as a title
 * @param referencedCollection Representation of [[ForeignKey.referencedCollection]]
 * @param referencedKey Representation of [[ForeignKey.referencedKey]] but only as a title
 */
case class Constraint(
    referencingCollection: String,
    referencingKey: String,
    referencedCollection: String,
    referencedKey: String
)
