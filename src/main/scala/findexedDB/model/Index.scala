package findexedDB.model

case class Index(
  name: String,
  //keys: Seq[Key],
  unique: Boolean
) extends CollectionOption
