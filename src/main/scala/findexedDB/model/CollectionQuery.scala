package findexedDB.model

import cats.effect.IO
import shapeless.{HList, _}
import ops.hlist.{Mapped, Zip, _}
import findexedDB.core.driver.Driver
import shapeless.ops.traversable._


/**
 * This class represents one transaction on a single Collection
 * @param driver Current Driver, for which we will be performing the request on Collection
 * @tparam T HList representing the type of
 */
class CollectionQuery[T <: HList, L <: HList, ZL <: HList](
    keyTitles: L,
    schema: CollectionSchema,
    zip: Zip.Aux[L :: T :: HNil, ZL],
    toTraversableZipped: ToTraversable.Aux[ZL, Seq, (KeySchema, Any)]
)(
    implicit driver: Driver
) {
    /**
     * Inserts a new record into the Collection
     * @param recordToInsert Product of type exactly as state in Collection definition which should be added to the database
     * @param gen Proof that recordToInsert satisfies T type type
     * @tparam P Actual Product type
     */
    def +=[P <: Product](recordToInsert: P)(implicit gen: Generic.Aux[P, T]): IO[Unit] = {
        val record = gen.to(recordToInsert)           // HList of exact values
        val zipped = zip.apply(keyTitles :: record :: HNil)
        val seq = toTraversableZipped.apply(zipped)

        driver.hookedInsert(schema.title, seq.map(value => (value._1.title, value._2)))
    }

    /**
     * The same as += but operates on Seq of records
     */
    def ++=[P <: Product](recordsToInsert: Seq[P])(implicit gen: Generic.Aux[P, T]): IO[Unit] = {
        val asSeq = recordsToInsert.map({ recordToInsert =>
            val record = gen.to(recordToInsert)
            val zipped = zip.apply(keyTitles :: record :: HNil)
            val seq = toTraversableZipped.apply(zipped)

            seq.map(value => (value._1.title, value._2))
        })
        driver.multipleInsert(schema.title, asSeq.toList)
    }

    def -=[TX](keyToDelete: TX): IO[Unit] = {
        driver.hookedDelete(schema.title, keyToDelete)
    }

    /**
     * Get all records from collection (order is presumed as in Database)
     * @param gen Proof that result records satisfy T type type
     * @param fromTraversable Function to create HList from Seq
     * @tparam P Actual Product type
     * @return
     */
    def all[P <: Product]()(implicit gen: Generic.Aux[P, T], fromTraversable: FromTraversable[T]): IO[List[P]] = {
        driver.getAll(schema.title).flatMap(list => IO(list.flatMap { seq =>
           val (titles, record) = seq.unzip

            fromTraversable.apply(record) match {
                case Some(res) => Some(gen.from(res))
                case None => None
            }
        }))
    }

    def exists[P <: Product](predicate: P => Boolean)(implicit gen: Generic.Aux[P, T], fromTraversable: FromTraversable[T]): IO[Boolean] = {
        filter[P](predicate).map(_.nonEmpty)
    }

    def filter[P <: Product](predicate: P => Boolean)(implicit gen: Generic.Aux[P, T], fromTraversable: FromTraversable[T]): IO[List[P]] = {
        driver.filter(schema.title, p => {
            val (titles, record) = p.unzip

            fromTraversable.apply(record) match {
                case Some(res) => predicate(gen.from(res))
            }
        }).flatMap(list => IO(list.map({ seq =>
            val (titles, record) = seq.unzip

            fromTraversable.apply(record) match {
                case Some(res) => gen.from(res)
            }
        })))
    }
}