package findexedDB.model

case class KeySchema (
    title: String,
    isPrimary: Boolean,
    isAutoInc: Boolean
)

/**
 * This is the representation of Collection for the Driver object
 * The main difference here is that it should contain only information required for the driver
 * @param title Actual name of collection for the
 * @param keys Just seq of strings, each string is just a name of key (later will have more info)
 * @param constraints TODO: docs
 */
case class CollectionSchema (
    title: String,
    keys: Seq[KeySchema],
    constraints: Seq[Constraint]
) {
    def getPrimaryKeyTitle: Option[String] =
        keys.find(_.isPrimary).map(_.title)

    /**
     * Checks if the collection is valid (e.g. no duplicate key titles)
     * @return
     */
    def isValid: Either[String, Unit] = {
        val err = List(
            ("empty title is forbidden",            title != ""),
            ("should be only one Primary Key",      keys.count(_.isPrimary) <= 1),
            ("no duplicate key titles are allowed", keys.distinctBy(_.title).length == keys.length)
        ).find(!_._2)

        err match {
            case Some((msg, _)) => Left(msg)
            case _ => Right()
        }
    }
}
