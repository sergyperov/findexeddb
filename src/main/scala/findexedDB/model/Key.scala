package findexedDB.model

trait KeyOptions

object Col {
    object IsAutoInc extends KeyOptions
    object IsPrimaryKey extends KeyOptions
}

trait Key[T] {
    val title: String
    val options: Seq[KeyOptions]
}

case class Col[T](
    title: String,
    options: KeyOptions*
) extends Key[T]

case class ColAutoInc[T](
    title: String,
    options: KeyOptions*
) extends Key[Option[T]]
