package findexedDB.model

/**
 * Final schema for the whole database, used by the Driver implementation in order to create a database instance based on this schema
 * @param title Any valid string
 * @param version We don't have a way to detect schema change on frontend (at least with IndexedDB) so version need to be specified explicitly
 * @param collections Sequence of collections schemas which will be used by the database
 */
case class DatabaseSchema (
    title: String,
    version: Int,
    collections: Seq[CollectionSchema]
) {
    val constraints: Seq[Constraint] = collections.flatMap(_.constraints)

    /**
     * Get all constraints where collectionTitle is referenced
     * @param collectionTitle referencedCollection (a.k.a. right-sided)
     * @return
     */
    def referencedConstraints(collectionTitle: String): Seq[Constraint] = constraints.filter(_.referencedCollection == collectionTitle)

    /**
     * Get all constraints where collectionTitle is referencing
     * @param collectionTitle referencingCollection (a.k.a. left-sided)
     * @return
     */
    def referencingConstraints(collectionTitle: String): Seq[Constraint] = constraints.filter(_.referencingCollection == collectionTitle)

    /**
     * Checking the whole collection schema (including constraints checks)
     * @return
     */
    def isValid: Either[String, Unit] = {
        val err = List(
            ("empty database title",                            title != ""),
            ("version must be positive integer number",         version >= 1),
            ("all collections must be valid by themselves",     collections.forall(_.isValid == Right())),
            ("check if al collection's titles are unique",      collections.distinctBy(_.title).length == collections.length),
            ("self-referencing collection",                     constraints.forall(c => c.referencedCollection != c.referencingCollection)),
            ("referencing collection doesn't exist",            constraints.forall(con => collections.exists(col => col.title == con.referencingCollection))),
            ("referenced collection doesn't exist",             constraints.forall(con => collections.exists(col => col.title == con.referencedCollection)))
        ).find(!_._2)

        err match {
            case Some((msg, _)) => Left(msg)
            case _ => Right()
        }
    }
}
