package findexedDB.model

import shapeless.{HList, _}
import ops.hlist.Mapped
import ops.hlist._
import findexedDB.model.CollectionQuery
import findexedDB.core.driver.Driver
import shapeless.Generic.Aux
import findexedDB.model.Col

import scala.language.implicitConversions

object keyTitle extends Poly1 {
    implicit def default[S, T](implicit ev : S <:< Key[T]) =
        at[S](col => {
            val isPrimary = col.options.contains(Col.IsPrimaryKey)

            KeySchema(col.title, isPrimary, isAutoInc = false)
        })
}

/**
 * Abstract class representing Collection ('table' in terms of SQL, 'ObjectStore' in terms of IndexedDB)
 * @tparam T representation of Collection's keys
 * @param title Collection Title
 */
abstract class Collection[T <: HList](title: String) {
    type OT <: HList
    type L <: HList
    type ZL <: HList

    // Evidence that parameters of Keys generics are exactly mapped T
    val mapped: Mapped.Aux[T, Key, OT]
    // Retrieving names from key object
    val keyNameMapper: Mapper.Aux[keyTitle.type, OT, L]
    // transforming HList of KeySchemas into Seq of KeySchemas
    val toTraversable: ToTraversable.Aux[L, Seq, KeySchema]
    // Zipping together names and actual data
    val zip: Zip.Aux[L :: T :: HNil, ZL]
    // Retrieving (recordTitle, recordData) sequence
    val toTraversableZipped: ToTraversable.Aux[ZL, Seq, (KeySchema, Any)]

    // HList that contains Key objects with different types as parameters
    def * : OT

    def $ : Seq[CollectionOption]

    def schema: CollectionSchema = {
        val res: Seq[KeySchema] = toTraversable.apply(keyNameMapper.apply(*))
        // for now ignore everything but ForeignKeys
        val constraints = $.filter(_ match {
            case ForeignKey(_, _, _) => true
            case _ => false
        }).map {
            case ForeignKey(key1, collection2, key2) => Constraint(title, key1.title, collection2, key2.title)
        }

        CollectionSchema(title, res, constraints)
    }

    def query(implicit driver: Driver) : CollectionQuery[T, L, ZL] = {
        new CollectionQuery[T, L, ZL](keyNameMapper.apply(*), schema, zip, toTraversableZipped)
    }
}

/**
 * Helper object, gives ability to set all implicits required for correct work with Shapeless
 */
object Collection {
    class PartiallyApplied[P <: Product] {
        def apply[P0 <: Product, T <: HList, OT0 <: HList, L0 <: HList, ZL0 <: HList](title: String)(x0: P0)(y0: Seq[CollectionOption] = Seq())(
            implicit gen: Generic.Aux[P, T],
            gen0: Generic.Aux[P0, OT0],
            mapped0: Mapped.Aux[T, Key, OT0],
            keyNameMapper0: Mapper.Aux[keyTitle.type, OT0, L0],
            toTraversable0: ToTraversable.Aux[L0, Seq, KeySchema],
            zip0: Zip.Aux[L0 :: T :: HNil, ZL0],
            toTraversableZipped0: ToTraversable.Aux[ZL0, Seq, (KeySchema, Any)]
        ): Collection[T] =
            new Collection[T](title) {
                type OT = OT0
                type L = L0
                type ZL = ZL0

                val mapped: Mapped.Aux[T, Key, OT] = mapped0
                val keyNameMapper: Mapper.Aux[keyTitle.type, OT, L] = keyNameMapper0
                val toTraversable: ToTraversable.Aux[L, Seq, KeySchema] = toTraversable0
                val zip: Zip.Aux[L :: T :: HNil, ZL] = zip0
                val toTraversableZipped: ToTraversable.Aux[ZL, Seq, (KeySchema, Any)] = toTraversableZipped0

                val * : OT = gen0.to(x0)
                val $ : Seq[CollectionOption] = y0
            }
    }

    def apply[P <: Product]: PartiallyApplied[P] = {
        new PartiallyApplied[P]
    }
}