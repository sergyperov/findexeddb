package findexedDB.core.driver

import cats.effect._
import findexedDB.model.DatabaseSchema

/**
 * Database Driver - special object which performs connection between Schema and practical Database implementation
 * This is some basic trait which should be extended by any real Driver
 *
 * Driver can have hooks so it extends Hookable
 */

trait Driver extends Hookable {
  val schema: DatabaseSchema

  /**
   * Should perform all work (including database/collections creation), resolves when ready
   * @return
   */
  def run(): IO[Unit]

  def hookedInsert(tableName: String, recordToInsert: Seq[(String, Any)]): IO[Unit] = {
    beforeInsertHook(tableName, recordToInsert)
        .flatMap(_ => insert(tableName, recordToInsert))
        .flatMap(_ => afterInsertHook(tableName, recordToInsert))
  }

  def hookedDelete(tableName: String, keyToDelete: Any): IO[Unit] = {
    beforeDeleteHook(tableName, keyToDelete)
        .flatMap(_ => delete(tableName, keyToDelete))
        .flatMap(_ => afterDeleteHook(tableName, keyToDelete))
  }

  /**
   * Should perform insertion of one element into given table
   * @param tableName Actual name of the table in the Database
   * @param recordToInsert Record that should be inserted into table
   * @return
   */
  def insert(tableName: String, recordToInsert: Seq[(String, Any)]): IO[Unit]

  /**
   * Should perform the same as [[insert]] but on multiple records
   * Implementation could be optimized if possible (for example, using some kind of bulk insert)
   * @param tableName Actual name of the table in the Database
   * @param recordsToInsert List records that should be inserted into table
   * @return
   */
  def multipleInsert(tableName: String, recordsToInsert: List[Seq[(String, Any)]]): IO[Unit]

  /**
   * Should perform insertion of one element into given table
   * @param tableName Actual name of the table in the Database
   * @param keyToDelete Key (that was given in table creation) the record with should be deleted
   * @return
   */
  def delete(tableName: String, keyToDelete: Any): IO[Unit]

  /**
   * Retrieves all records from a given table
   * @param tableName Actual name of the table in the Database
   * @return List of Sequences, where each sequence represents a records
   */
  def getAll(tableName: String): IO[List[Seq[(String, Any)]]]

  def filter(tableName: String, predicate: Seq[(String, Any)] => Boolean): IO[List[Seq[(String, Any)]]]

  def exists(tableName: String, f: Seq[(String, Any)] => Boolean): IO[Boolean]

  def all(tableName: String, f: Seq[(String, Any)] => Boolean): IO[Boolean]

}
