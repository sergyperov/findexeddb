package findexedDB.core.driver

import cats.effect.IO

/**
 * Hookable describes any class that has Hooks
 *
 * Hooks are some sort of async procedures which can be performed before and after any *modifying* actions
 * Any request will later be chained as: beforeHook -> event -> afterHook
 * Because of mention sequence beforeHook can be used to prevent some action on the database (by arising error)
 */
trait Hookable {
    def beforeInsertHook(tableName: String, recordToInsert: Seq[(String, Any)]): IO[Unit]
    def afterInsertHook(tableName: String, recordToInsert: Seq[(String, Any)]): IO[Unit]

    def beforeDeleteHook(tableName: String, keyToDelete: Any): IO[Unit]
    def afterDeleteHook(tableName: String, keyToDelete: Any): IO[Unit]
}
