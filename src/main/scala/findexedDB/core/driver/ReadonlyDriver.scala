package findexedDB.core.driver

import cats.effect.IO

/**
 * Driver which restricts any kind of data modify. Note that is extends BasicDriver and override all "before" hooks
 * We don't need to change "after" hooks because they will never be executed due to the error raised by "before" hooks
 */
abstract class ReadonlyDriver extends BasicDriver {
    override def beforeInsertHook(tableName: String, recordToInsert: Seq[(String, Any)]): IO[Unit] =
        IO.raiseError(new Exception("Insertions are restricted in readonly driver"))
}
