package findexedDB.core.driver

import cats.effect.IO

/**
 * Some basic implementation of hooks where each hook just processes next command
 */
abstract class BasicDriver extends Driver {
    def beforeInsertHook(tableName: String, recordToInsert: Seq[(String, Any)]): IO[Unit] = IO.unit
    def afterInsertHook(tableName: String, recordToInsert: Seq[(String, Any)]): IO[Unit] = IO.unit

    def beforeDeleteHook(tableName: String, keyToDelete: Any): IO[Unit] = IO.unit
    def afterDeleteHook(tableName: String, keyToDelete: Any): IO[Unit] = IO.unit
}
