package findexedDB.core.driver

import cats.effect.IO

abstract class ConstraintDriver extends BasicDriver {
    override def beforeInsertHook(tableName: String, recordToInsert: Seq[(String, Any)]): IO[Unit] = {
        val constraints = schema.referencingConstraints(tableName)
        if (constraints.nonEmpty) {
            val constraint = constraints.head
            recordToInsert.toMap.get(constraint.referencingKey) match {
                case Some(value) =>
                    return exists(constraint.referencedCollection, seq => {
                        seq.toMap.get(constraint.referencedKey) match {
                            case Some(valueToCompare) => valueToCompare == value
                            case None => true
                        }
                    }).flatMap {
                        case true => IO.unit
                        case false => IO.raiseError(
                            new Exception(s"[ConstraintDriver] No reference for key '${constraint.referencingCollection}.${constraint.referencingKey}' found among '${constraint.referencedCollection}.${constraint.referencedKey}'.")
                        )
                    }
            }
        }

        IO.unit
    }

    override def beforeDeleteHook(tableName: String, keyToDelete: Any): IO[Unit] = {
        val constraints = schema.referencedConstraints(tableName)
        if (constraints.nonEmpty) {
            val constraint = constraints.head
            return all(constraint.referencingCollection, seq => {
                seq.toMap.get(constraint.referencingKey) match {
                    case Some(valueToCompare) => valueToCompare != keyToDelete
                    case None => false
                }
            }).flatMap {
                case true => IO.unit
                case false => IO.raiseError(
                    new Exception(s"[ConstraintDriver] Key '${constraint.referencedCollection}.${constraint.referencedKey}' has references among '${constraint.referencingCollection}.${constraint.referencingKey}'.")
                )
            }
        }

        IO.unit
    }
}
