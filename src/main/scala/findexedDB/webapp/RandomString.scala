package findexedDB.webapp

import scala.annotation.tailrec

object RandomString {
    // 3 - recursive, but not tail-recursive
    def randomStringRecursive(n: Int): List[Char] = {
        n match {
            case 1 => List(util.Random.nextPrintableChar)
            case _ => List(util.Random.nextPrintableChar) ++ randomStringRecursive(n-1)
        }
    }

    // 3b - recursive, but not tail-recursive
    def randomStringRecursive2(n: Int): String = {
        n match {
            case 1 => util.Random.nextPrintableChar.toString
            case _ => util.Random.nextPrintableChar.toString ++ randomStringRecursive2(n-1).toString
        }
    }

    // 4 - tail recursive, no wrapper
    @tailrec
    def randomStringTailRecursive(n: Int, list: List[Char]):List[Char] = {
        if (n == 1) util.Random.nextPrintableChar :: list
        else randomStringTailRecursive(n-1, util.Random.nextPrintableChar :: list)
    }

    // 5 - a wrapper around the tail-recursive approach
    def randomStringRecursive2Wrapper(n: Int): String = {
        randomStringTailRecursive(n, Nil).mkString
    }

    // 6 - random alphanumeric
    def randomAlphaNumericString(length: Int): String = {
        val chars = ('a' to 'z') ++ ('A' to 'Z') ++ ('0' to '9')
        randomStringFromCharList(length, chars)
    }

    // 7 - random alpha
    def randomAlpha(length: Int): String = {
        val chars = ('a' to 'z') ++ ('A' to 'Z')
        randomStringFromCharList(length, chars)
    }

    // used by #6 and #7
    def randomStringFromCharList(length: Int, chars: Seq[Char]): String = {
        val sb = new StringBuilder
        for (i <- 1 to length) {
            val randomNum = util.Random.nextInt(chars.length)
            sb.append(chars(randomNum))
        }
        sb.toString
    }

    def x(length: Int, chars: Seq[Char]): String = {
        val list = List.range(1, length)
        val arr = new Array[Char](length)
        list.foreach{ e => arr(e) = chars(util.Random.nextInt(chars.length)) }
        list.mkString
    }

    // create a fake list so i can use map (or flatMap)
    def x2(length: Int, chars: Seq[Char]): String = {
        val tmpList = List.range(0, length)
        val charList = tmpList.map{ e => chars(util.Random.nextInt(chars.length)) }
        return charList.mkString
    }
}
