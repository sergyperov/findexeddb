package findexedDB.webapp

import cats.effect.IO
import org.scalajs.dom
import dom.document
import findexedDB.implementation.IndexedDB.IndexedDBDriver
import findexedDB.model.{Col, Collection, DatabaseSchema, ForeignKey, Index, Key}
import org.scalajs.dom.raw.Event

object User {
    // list of keys
    def id: Key[Int] = Col[Int]("id", Col.IsPrimaryKey)  // indicates primary key
    def name: Key[String] = Col[String]("name")
    private val * = (id, name)

    private val $ = Seq()

    // collection definition
    def collection = Collection[(Int, String)]("Users")(*)($)
    def getCollectionTitle: String = collection.schema.title
}

object Item {
    // list of keys
    def id: Key[Int] = Col[Int]("id", Col.IsPrimaryKey)
    def title: Key[String] = Col[String]("title")
    def price: Key[Double] = Col[Double]("price")
    def owner: Key[Int] = Col[Int]("owner")
    private val * = (id, title, price, owner)

    def foreignKeyOwner: ForeignKey[Int] = ForeignKey(owner, User.getCollectionTitle, User.id)
    private val $ = Seq(foreignKeyOwner)

    // collection definition
    def collection = Collection[(Int, String, Double, Int)]("Items")(*)($)
}

object WebFormController {
    def init(): Unit = {
        val dbSchema = DatabaseSchema("Testable database", 1,
            Seq(
                User.collection.schema,
                Item.collection.schema
            )
        )

        implicit val driver: IndexedDBDriver = new IndexedDBDriver(dbSchema)

        val userDataContainer = document.createElement("div")
        userDataContainer.classList.add("data-container")
        userDataContainer.classList.add("user-data-container")
        val itemDataContainer = document.createElement("div")
        itemDataContainer.classList.add("data-container")
        itemDataContainer.classList.add("item-data-container")
        val notificationElement = document.createElement("div")
        val notification = new Notification(notificationElement)
        notification.init()
        document.body.appendChild(userDataContainer)
        document.body.appendChild(itemDataContainer)
        document.body.appendChild(notificationElement)

        val webForm = new WebForm(userDataContainer, itemDataContainer, notification)
        webForm.init()
    }
}

class WebForm(
    userDataContainer: dom.Element,
    itemDataContainer: dom.Element,
    notification: Notification
)
(implicit val driver: IndexedDBDriver) {
    def init(): Unit = {
        driver.run().unsafeRunAsync(cb => {
            renderUsersData()
            renderItemsData()
        })
    }

    def renderUsersData(): Unit = {
        userDataContainer.innerHTML = ""
        val getAll = User.collection.query.all[(Int, String)]()
        getAll.flatMap { res: List[(Int, String)] =>
            val table = TableForm.tableWithInsertion(
                List("ID", "Name"),
                res.map {case (id: Int, name: String) => List(id.toString, name) },
                TableRowAction("Delete", (list: List[String]) => {
                    toInt(list.head) match {
                        case Some(id) => {
                            println("Deleting", id)
                            val deleteUserQuery = User.collection.query -= id
                            runWithHandle(deleteUserQuery)(renderUsersData)
                        }
                    }
                }),
                (values: List[String]) => {
                    val id = toInt(values.head)
                    val name = values(1)

                    id match {
                        case Some(value: Int) =>
                            val insertUserQuery = User.collection.query += (value, name)
                            runWithHandle(insertUserQuery)(renderUsersData)
                        case None => reportFailure("Error! ID is not an Integer")
                    }
                }
            )
            userDataContainer.appendChild(table)

            IO.unit
        }.unsafeRunAsyncAndForget()
    }

    def renderItemsData(): Unit = {
        itemDataContainer.innerHTML = ""
        val getAll = Item.collection.query.all[(Int, String, Double, Int)]()
        getAll.flatMap { res: List[(Int, String, Double, Int)] =>
            val table = TableForm.tableWithInsertion(
                List("ID", "Title", "Price ($)", "Owner User ID"),
                res.map {case (id: Int, title: String, price: Double, ref: Int) => List(id.toString, title, price.toString, ref.toString) },
                TableRowAction("Delete", (list: List[String]) => {
                    toInt(list.head) match {
                        case Some(id) => {
                            val deleteItemQuery = Item.collection.query -= id
                            runWithHandle(deleteItemQuery)(renderItemsData)
                        }
                    }
                }),
                (values: List[String]) => {
                    val id = toInt(values.head)
                    val title = values(1)
                    val price = toDouble(values(2))
                    val ownerId = toInt(values(3))

                    id match {
                        case Some(_id: Int) => price match {
                            case Some(_price: Double) => ownerId match {
                                case Some(_ownerId: Int) => {
                                    val insertItemQuery = Item.collection.query += (_id, title, _price, _ownerId)
                                    runWithHandle(insertItemQuery)(renderItemsData)
                                }
                                case None => reportFailure("Error! OwnerID is not an Integer")
                            }
                            case None => reportFailure("Error! Price is not a Number")
                        }
                        case None => reportFailure("Error! ID is not an Integer")
                    }
                }
            )
            itemDataContainer.appendChild(table)

            IO.unit
        }.unsafeRunAsyncAndForget()
    }

    def runWithHandle(query: IO[Unit])(cb: () => Unit) = {
        query.flatMap(_ => IO{
            reportSuccess("Request was completed successfully.")
            cb()
        }).handleErrorWith(e => IO(
            reportFailure("Error during request: " + e.getMessage)
        )).unsafeRunAsyncAndForget()
    }

    def reportSuccess(message: String): Unit = {
        notification.show(message, "success")
    }

    def reportFailure(message: String): Unit = {
        notification.show(message, "failure")
    }

    def toInt(s: String): Option[Int] = {
        try {
            Some(s.toInt)
        } catch {
            case e: Exception => None
        }
    }

    def toDouble(s: String): Option[Double] = {
        try {
            Some(s.toDouble)
        } catch {
            case e: Exception => None
        }
    }
}

object InputForm {
    def form(captions: List[String], submitCaption: String, onSubmit: List[String] => Unit): dom.Element = {
        val form = document.createElement("form")
        val table = document.createElement("table")

        val inputElements: List[dom.html.Input] = inputs(captions)
        inputElements.zipWithIndex.foreach{case (inputElement: dom.html.Input, i: Int) =>
            val label = document.createElement("label")
            label.setAttribute("for", inputElement.id)
            label.appendChild(document.createTextNode(captions(i)))

            table.appendChild(TableForm.row(List(label, inputElement)))
        }

        form.appendChild(table)

        val button = document.createElement("button")
        button.appendChild(document.createTextNode(submitCaption))
        form.appendChild(button)

        form.addEventListener("submit", (e: dom.Event) => {
            e.preventDefault()
            e.stopPropagation()

            val values = inputElements.map(input => input.value)
            onSubmit(values)
        })

        form
    }

    def inputs(captions: List[String]): List[dom.html.Input] = {
        val id: String = "elem-" + RandomString.randomAlphaNumericString(10)
        val inputs = captions.zipWithIndex.map {case (caption: String, i: Int) =>
            (caption, id + "-input" + i)
        }

        val inputElements: List[dom.html.Input] = inputs.map {case (caption: String, inputId: String) =>
            val input = document.createElement("input")
            input.setAttribute("id", inputId)
            input.setAttribute("name", inputId)

            input.asInstanceOf[dom.html.Input]
        }

        inputElements
    }
}

class Notification (element: dom.Element) {
    def init(): Unit = {
        element.classList.add("notification-popup")
    }

    def show(message: String, styling: String): Unit = {
        element.classList.remove("failure")
        element.classList.remove("success")

        element.classList.add("visible")
        element.classList.add(styling)
        element.innerText = message;

        dom.window.setTimeout(() => {
            element.classList.remove("visible")
        }, 3000)

        ()
    }
}

case class TableRowAction[T] (caption: String, handler: T => Unit)

object TableForm {
    def textNodes(values: List[String]): List[dom.Element] = {
        values.map(value => document.createTextNode(value).asInstanceOf[dom.Element])
    }

    def _row(titles: List[dom.Element], rowElem: String): dom.Element = {
        val tableRow = document.createElement("tr")
        titles.foreach(title => {
            val tableCol = document.createElement(rowElem)
            tableCol.appendChild(title)
            tableRow.appendChild(tableCol)
        })

        tableRow
    }

    def headerRow(titles: List[dom.Element]): dom.Element = _row(titles, "th")

    def row(titles: List[dom.Element]): dom.Element = _row(titles, "td")

    def button(caption: String, onClick: () => Unit): dom.Element = {
        val button = document.createElement("button")
        button.appendChild(document.createTextNode(caption))
        button.addEventListener("click", (e: Event) => onClick())

        button
    }

    def table(headers: List[String], values: List[List[String]], action: TableRowAction[List[String]]): dom.Element = {
        val table = document.createElement("table")
        val header = headerRow(textNodes(headers.appended("Actions")))
        table.appendChild(header)
        values.foreach(rowData => {
            val row = TableForm.row(textNodes(rowData).appended(button(action.caption, () => action.handler(rowData))))
            table.appendChild(row)
        })

        table
    }

    def tableWithInsertion(headers: List[String], values: List[List[String]], action: TableRowAction[List[String]], onSubmit: List[String] => Unit): dom.Element = {
        val _table = table(headers, values, action)

        val inputElements: List[dom.html.Input] = InputForm.inputs(headers)
        val insertionRow: List[dom.Element] = inputElements.appended(
            button("Insert", () => {
                val values = inputElements.map(input => input.value)
                onSubmit(values)
            })
        )
        _table.appendChild(row(insertionRow))

        _table
    }
}