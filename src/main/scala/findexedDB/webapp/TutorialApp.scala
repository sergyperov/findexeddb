package findexedDB.webapp

import cats.effect.IO
import findexedDB.model.{Col, Collection, Key, DatabaseSchema, ForeignKey, Index}
import findexedDB.implementation.IndexedDB.IndexedDBDriver

import scala.language.implicitConversions

object ModelTest {
    def init(): Unit = {
        val dbSchema = DatabaseSchema("Testable database", 1,
            Seq(
                User.collection.schema,
                Item.collection.schema
            )
        )

        implicit val driver: IndexedDBDriver = new IndexedDBDriver(dbSchema)

        driver.run().unsafeRunAsync(cb => {
            val insertUserQuery = User.collection.query += (991, "some user")
            val insertProductQuery = Item.collection.query += (404, "Some item", 44.9, 991)
            val multipleUserInsertQuery = User.collection.query ++= Seq(
                (992, "user 405"),
                (993, "user 406"),
                (994, "user 407"),
                (995, "user 408"),
                (996, "user 409"),
                (997, "user 410")
            )
//            insertUserQuery.flatMap(_ => IO(println("done"))).handleErrorWith(e => IO(println("got error", e.getMessage))).unsafeRunAsyncAndForget()
            insertUserQuery.unsafeRunAsyncAndForget();
            insertProductQuery.unsafeRunAsyncAndForget()
            multipleUserInsertQuery.unsafeRunAsyncAndForget()

            val geAll = User.collection.query.all[(Int, String)]()
            geAll.flatMap { res: List[(Int, String)] =>
                IO(println(res))
            }.unsafeRunAsyncAndForget()

        })
    }
}



object TutorialApp extends App {
    //ModelTest.init()
    WebFormController.init();
}
