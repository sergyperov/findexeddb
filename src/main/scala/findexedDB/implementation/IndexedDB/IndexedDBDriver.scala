package findexedDB.implementation.IndexedDB

import cats.effect.IO
import findexedDB.core.driver.{ConstraintDriver}
import findexedDB.implementation.IndexedDB.wrappers.{Database, IndexedDB}
import findexedDB.model.DatabaseSchema

import scala.collection.mutable
import scala.scalajs.js

/**
 * Concrete implementation of Driver for IndexedDB
 * @param dbSchema Some ready database schema which will be used by the Driver
 */
class IndexedDBDriver(val dbSchema: DatabaseSchema) extends ConstraintDriver {
    override val schema: DatabaseSchema = dbSchema

    override def run(): IO[Unit] = {
        schema.isValid match {
            case Left(msg) => IO.raiseError(
                new Exception(msg)
            )
            case _ => IO.async { cb =>
                val dbIO: IO[Database] = IndexedDB.open(schema.title, schema.version) { (connection, transaction) =>
                    // TODO: perform key check, remove some values if key was deleted from Schema
                    // We can't really specify keys here, so keys are still some kind of abstraction for this moment
                    schema.collections.foreach(colSchema => {
                        colSchema.getPrimaryKeyTitle match {
                            case Some(primaryKey) =>
                                // we have PrimaryKey specified, so pass it as keyPath
                                connection.createObjectStore(colSchema.title, js.Dictionary("keyPath" -> primaryKey))
                            case None =>
                                // TODO: find some proper way to handle collections with no primary keys
                                connection.createObjectStore(colSchema.title, js.Dictionary("keyPath" -> ""))
                        }
                    })
                }
                dbIO.unsafeRunAsync({_ =>
                    cb(Right())
                })
            }
        }
    }

    def insert(tableName: String, recordToInsert: Seq[(String, Any)]): IO[Unit] = {
        val dbIO: IO[Database] = IndexedDB.open(schema.title, schema.version)((_,_) => ())

        dbIO.flatMap({ db =>
            val transaction = db.transaction(tableName, "readwrite")
            val objectStore = transaction.objectStore(tableName)
            val jsObject = js.Dictionary(recordToInsert:_*)

            objectStore.add(jsObject)
        })
    }

    def delete(tableName: String, keyToDelete: Any): IO[Unit] = {
        val dbIO: IO[Database] = IndexedDB.open(schema.title, schema.version)((_,_) => ())

        dbIO.flatMap({ db =>
            val transaction = db.transaction(tableName, "readwrite")
            val objectStore = transaction.objectStore(tableName)

            objectStore.delete(keyToDelete.asInstanceOf[js.Any])
        })
    }

    def multipleInsert(tableName: String, recordsToInsert: List[Seq[(String, Any)]]): IO[Unit] = {
        // indexedDB doesn't provide any kind of 'bulk insertion' mechanism so we are just adding values in a async sequence
        recordsToInsert.foldRight(IO.pure())((record, insertIO) => insertIO.flatMap(Unit => insert(tableName, record)))
    }

    def getAll(tableName: String): IO[List[Seq[(String, Any)]]] = {
        val someDB: IO[Database] = IndexedDB.open(schema.title, schema.version)((_,_) => ())
        someDB.flatMap({ db =>
            val transaction = db.transaction(tableName, "readwrite")
            val objectStore = transaction.objectStore(tableName)

            objectStore.getAll
        }).map({ list =>
            list.map(res => {
                val jsDict = res.asInstanceOf[js.Dictionary[Any]]
                val map: mutable.Map[String, Any] = jsDict
                map.toSeq
            })
        })
    }

    def filter(tableName: String, f: Seq[(String, Any)] => Boolean): IO[List[Seq[(String, Any)]]] = {
        val someDB: IO[Database] = IndexedDB.open(schema.title, schema.version)((_,_) => ())
        someDB.flatMap({ db =>
            val transaction = db.transaction(tableName, "readwrite")
            val objectStore = transaction.objectStore(tableName)

            objectStore.filter(IndexedDBDriver.booleanMap(f)(_))
        }).map({ list =>
            list.map(res => {
                val jsDict = res.asInstanceOf[js.Dictionary[Any]]
                val map: mutable.Map[String, Any] = jsDict
                map.toSeq
            })
        })
    }

    def exists(tableName: String, f: Seq[(String, Any)] => Boolean): IO[Boolean] = {
        val someDB: IO[Database] = IndexedDB.open(schema.title, schema.version)((_,_) => ())
        someDB.flatMap({ db =>
            val transaction = db.transaction(tableName, "readwrite")
            val objectStore = transaction.objectStore(tableName)

            objectStore.exists(IndexedDBDriver.booleanMap(f)(_))
        })
    }

    def all(tableName: String, f: Seq[(String, Any)] => Boolean): IO[Boolean] = {
        val someDB: IO[Database] = IndexedDB.open(schema.title, schema.version)((_,_) => ())
        someDB.flatMap({ db =>
            val transaction = db.transaction(tableName, "readwrite")
            val objectStore = transaction.objectStore(tableName)

            objectStore.all(res => IndexedDBDriver.booleanMap(f)(res))
        })
    }
}

object IndexedDBDriver {
    def booleanMap(f: Seq[(String, Any)] => Boolean)(item: js.Any): Boolean = {
        val jsDict = item.asInstanceOf[js.Dictionary[Any]]
        val map: mutable.Map[String, Any] = jsDict

        f(map.toSeq)
    }
}
