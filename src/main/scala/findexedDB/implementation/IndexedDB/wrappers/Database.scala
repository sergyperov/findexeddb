package findexedDB.implementation.IndexedDB.wrappers

import org.scalajs.dom.idb

/**
 * Facade that represents simpler access to IndexedDB API wrapped with IO
 */
class Database(db: idb.Database) extends DatabaseInstance {
    def transaction(storeNames: scala.scalajs.js.Any, mode: String): Transaction = new Transaction(db.transaction(storeNames, mode))
}
