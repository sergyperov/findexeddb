package findexedDB.implementation.IndexedDB.wrappers

import org.scalajs.dom.idb

/**
 * Facade on IndexedDB Transaction object
 * @param transaction IndexedDB Transaction object
 */
class Transaction(transaction: idb.Transaction) extends DatabaseInstance {
    def objectStore(storeName: String): ObjectStore = new ObjectStore(transaction.objectStore(storeName))
}
