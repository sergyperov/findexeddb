package findexedDB.implementation.IndexedDB.wrappers

/**
 * Simple facade for IndexedDB methods, giving ability to work with Cats Effects (especially with IO)
 * findexedDB library is supposed to work only with IO-like events so it will be using this facade
 *
 * Not all of the methods are decorated for now, more is coming!
 * Note: this facade doesn't provide any type safety
 */

import cats.effect._
import org.scalajs.dom.idb
import org.scalajs.dom.raw.IDBVersionChangeEvent

import scala.scalajs.js

object IndexedDB extends DatabaseInstance {
	def open(name: String, version: Int = 1)(upgradeHandler: (idb.Database, idb.Transaction) => Unit): IO[Database] = {
		IO.async { cb =>
			val request = factory.open(name, version)
			request.onupgradeneeded = {(event: IDBVersionChangeEvent) =>
				val db = event.target.asInstanceOf[js.Dynamic].result.asInstanceOf[idb.Database]
				val transaction = event.currentTarget.asInstanceOf[js.Dynamic].transaction.asInstanceOf[idb.Transaction]
				upgradeHandler(db, transaction)
			}

			proceedRequest(request)(cb)((event, _) => {
				val db = event.target.asInstanceOf[js.Dynamic].result.asInstanceOf[idb.Database]
				new Database(db)
			})
		}
	}

	private def factory: idb.Factory = js.Dynamic.global.indexedDB.asInstanceOf[idb.Factory]
}
