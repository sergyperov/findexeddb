package findexedDB.implementation.IndexedDB.wrappers

import cats.effect.IO
import org.scalajs.dom.Event
import org.scalajs.dom.raw.{IDBCursorWithValue, IDBRequest}

sealed trait Cursor

case object NilCursor extends Cursor

/**
 * Cursor is actually the hardest part of IndexedDB to express
 * It is basically an iterator, so it is expressed using recursive case class with method next(), return next cursor if possible
 * @param request Current IDB openCursor() request
 * @param value Value associated with current cursor
 */
case class ValueCursor(request: IDBRequest, value: scala.scalajs.js.Any) extends Cursor {
    def next(): IO[Cursor] = {
        IO.async({ cb =>
            // onsuccess will be fired exact one time in each class instance because we are recursively creating new `ClueCursor`s where callback is overwritten
            request.onsuccess = {(event: Event) =>
                event.target.asInstanceOf[IDBRequest].result match {
                    case cursor: IDBCursorWithValue =>
                        val cursorValue = cursor.value
                        cursor.continue()
                        cb(Right(ValueCursor(request, cursorValue)))
                    case _ =>
                        cb(Right(NilCursor))
                }
            }
        })
    }
}

object Cursor {
    def apply(request: IDBRequest): IO[Cursor] = {
        ValueCursor(request, null).next()
    }

    def foldLeft[B](fa: IO[Cursor], b: IO[B])(f: (IO[B], scala.scalajs.js.Any) => IO[B]): IO[B] = {
        fa.flatMap({
            case valueCursor: ValueCursor =>
                f(foldLeft(valueCursor.next(), b)(f), valueCursor.value)
            case NilCursor => b
        })
    }
}




