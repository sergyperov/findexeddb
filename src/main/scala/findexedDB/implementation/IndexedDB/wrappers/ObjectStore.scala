package findexedDB.implementation.IndexedDB.wrappers

import cats.effect.IO
import org.scalajs.dom.idb

import scala.scalajs.js

/**
 * Facade on IndexedDB ObjectStore object
 * @param objectStore IndexedDB ObjectStore object which will be wrapped
 */
class ObjectStore(objectStore: idb.ObjectStore) extends DatabaseInstance {
    def add(value: js.Any): IO[Unit] = {
        IO.async { cb =>
            proceedRequest(objectStore.add(value))(cb)((_,_) =>())
        }
    }

    def delete(key: js.Any): IO[Unit] = {
        IO.async{ cb =>
            proceedRequest(objectStore.delete(key))(cb)((_,_) =>())
        }
    }

    def put(value: js.Any): IO[Unit] = {
        IO.async { cb =>
            proceedRequest(objectStore.put(value))(cb)((_,_) =>())
        }
    }

    def get(key: js.Any): IO[js.Any] = {
        IO.async {cb =>
            proceedRequest(objectStore.get(key))(cb)((_,req) => req.result)
        }
    }

    def getAll: IO[List[js.Any]] = {
        val cursor = Cursor.apply(objectStore.openCursor())
        Cursor.foldLeft[List[js.Any]](cursor, IO.pure(List.empty))({ (listIO, value) =>
            // we are prepending elements because cursors are evaluating from last to first
            listIO.map(list => list.+:(value))
        })
    }

    def filter(f: js.Any => Boolean): IO[List[js.Any]] = {
        val cursor = Cursor.apply(objectStore.openCursor())
        Cursor.foldLeft[List[js.Any]](cursor, IO.pure(List.empty))({ (listIO, value) =>
            // we are prepending elements because cursors are evaluating from last to first
            if (f(value)) {
                listIO.map(list => list.+:(value))
            } else {
                listIO
            }
        })
    }

    def exists(f: js.Any => Boolean): IO[Boolean] = predicate(f)(initial = false, (bool, value) => bool || f(value));

    def all(f: js.Any => Boolean): IO[Boolean] = predicate(f)(initial = true, (bool, value) => bool && f(value));

    def predicate(f: js.Any => Boolean)(initial: Boolean, mapper: (Boolean, js.Any) => Boolean): IO[Boolean] = {
        val cursor = Cursor.apply(objectStore.openCursor())
        Cursor.foldLeft[Boolean](cursor, IO.pure(initial))({ (booleanIO, value) =>
            booleanIO.map(bool => mapper(bool, value))
        })
    }
}
