package findexedDB.implementation.IndexedDB.wrappers

import org.scalajs.dom.{ErrorEvent, Event, idb}
import org.scalajs.dom

/**
 * Basic trait which should be inherited by all IndexedDB classes
 * Has some nice methods to simplify general code and remove some boiler plate
 */
trait DatabaseInstance {
    def proceedRequest[A](request: idb.Request)(cb: Either[Throwable, A] => Unit)(resolve: (Event, idb.Request) => A): Unit = {
        request.onsuccess = {(event: Event) =>
            cb(Right(resolve(event, request)))
        }
        request.onerror = {_ =>
            cb(Left(new Exception(request.error.message)))
        }
    }
}
