enablePlugins(ScalaJSPlugin)

name := "findexedDB"
scalaVersion := "2.13.1"

mainClass := Some("findexedDB.webapp.TutorialApp")

libraryDependencies ++= {
  Seq(
    "org.typelevel" %%% "cats-core" % "2.1.1",
    "org.typelevel" %%% "cats-effect" % "2.1.2",
    "com.chuusai" %%% "shapeless" % "2.3.3",
    "org.scala-js" %%% "scalajs-dom" % "1.0.0",
    "com.lihaoyi" %%% "utest" % "0.7.4" % "test",
  )
}

testFrameworks += new TestFramework("utest.runner.Framework")