# findexedDB — Wrapper for IndexedDB with typed schema and constraints support

Functional wrapper for IndexedDB written in [scala.js][^1] using [Shapeless][^2] and [Cats-Effect][^3]. [uTest][^4] is
used for testing.

## Architecture

Database consists of the following components:

* **Record:** some unit of data, can be a simple type (like Int or String), or a complex object (like Case class), each
value is called a **Key**
* **Collection:** group of records with the same type, can have a **Primary Key** and additional **Indexes**
* **Database:** a group of collections

All of this is some kind of wrappers on indexedDB:

| findexedDB | IndexedDB representation |
| --- | --- |
| Record | Object in IDBObjectStore |
| Collection | IDBObjectStore |
| Database | IDBDatabase |

## Modelling a Database

### Collection Schema

When modelling a Database, you start with defining collections of which your database will consist. You can use
`Collection` class to create a collection schema:

```scala
val keys = (
    Col[Int]("someIntKey"),
    Col[String]("someStringKey")
)
val constraints = Seq()
val collection = Collection[(Int, String)]("SimpleCollection")(keys)(constraints)
```

Here we left `constraints` empty, this feature will be discussed later. Note that collection is strongly typed and
you need to provide `keys` according to the given `Collection[T]` type. 

#### Key parameters

It is also worth mentioning that you can provide additional arguments (flags) when creating a key:

```scala
Col[Int]("someIntKey", Col.PrimaryKey)
```

This will make a particular key a `Primary Key`.

### Database Schema

Database Schema consists of Database name, Database version and a list of `Collection`s:

```scala
val dbSchema = DatabaseSchema("some database", 1, Seq(
    firstCollection.schema,
    secondCollection.schema
))
```

## Database Driver

`DatabaseSchema` (and all it's `Collection`s) is just some abstract description of data. In order to perform any actions
you need a `Database Driver`. `Driver` itself requires a `DatabaseSchema` which physical implementation it will create.
For now, there is only one driver available — `IndexedDBDriver`. So to create a `Driver`:
                                                                                         
```scala
implicit val driver: IndexedDBDriver = new IndexedDBDriver(dbSchema)
```

You can use `Driver`s `run` method to initialise it:

```scala
driver.run().flatMap({
    // here driver is ready
})
```

## Queries

One of the concepts of this library is that queries are performed towards individual `Collection`s, not to
the entire `Database`. Moreover, because all queries are asynchronous, `IO monad` is used to handle query operations.
Each `Collection` has a special `query` field which can be used to execute a query on a particular `Collection`. `query`
itself requires an `implicit` driver: this way it knows which implementation should be used for the query. Therefore it
is more convenient to declare `driver` as `implicit`.

### query.+=

Adds new `Record` into the `Collection`.

```scala
collection.query += (1, "Testing")
```

### query.++=

Adds a `Seq` of `Record`s into the `Collection`.

```scala
collection.query ++= Seq(
    (1, "Hello"),
    (2, "world")
)
```

### query.-=

Removes a specific `Record` from the `Collection` by `PrimaryKey`.

```scala
collection.query -= id
```

### query.all

Get all `Record`s from the `Collection`.

```scala
collection.query.all[(Int, String)]().flatMap { res: List[(Int, String)] =>
    // res with all the records
}
```

### query.filter

Get `Record`s from the `Collection` which satisfy some predicate.

```scala
collection.query.filter[(Int, String)](_ < 100).flatMap { res: List[(Int, String)] =>
    // res with records where id < 100
}
```

## Integrity constraint support

Library has support for integrity constraints. You create a constraint by creating a `ForeignKey`:

```scala
ForeignKey("keyA", "collectionY", "keyB")
```

If this ForeignKey was declared within `collectionX`, it creates a constraint `collectionX.keyA -> collectionY.keyB`.
Therefore, no need to specify `collectionX` because it is detected automatically.

### Schema errors handling

Look at the following code:

```scala
val keys = (
    Col[Int]("someIntKey"),
    Col[String]("someStringKey")
)

val constraints = Seq(
    ForeignKey("someIntKey", "AnotherCollection", "someParentKey")
)

val collection = Collection[(Int, String)]("SimpleCollection")(keys)(constraints)
```

If there are no keys like `someIntKey` or no collection as `AnotherCollection`, `IO error` will be raised during `run()`
method.

### Query errors handling

Before any query which *changes* any store of any Collection in the Database, Integrity check will be performed.
If incoming query will break any of constrains in Database Schema, `IO error` will be raised.

[^1]: http://www.scala-js.org
[^2]: https://github.com/milessabin/shapeless
[^3]: https://typelevel.org/cats-effect/
[^4]: https://github.com/lihaoyi/utest